<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Chrome, Firefox OS, Opera and Vivaldi -->
  <meta name="theme-color" content="#7fa5c3">
  <!-- Windows Phone -->
  <meta name="msapplication-navbutton-color" content="#7fa5c3">
  <!-- iOS Safari -->
  <meta name="apple-mobile-web-app-status-bar-style" content="#7fa5c3">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>Bienvenido a WTFE</title>
  <link rel="shortcut icon" href="images/favico.ico">

  <!-- Bootstrap -->
  <link href="css/bootstrap.css" rel="stylesheet">
  <link href="css/theme.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
  <body>
  <div class="wrapp">
    <ul>
      <?php
          echo "<h1>Plantillas:</h1> <br /><br />";
          $path = ".";
          $dh = opendir($path);
          $i=1;
          while (($file = readdir($dh)) !== false) {
        if($file != "." && $file != ".." && $file != "index.php" && $file != ".htaccess" && $file != "error_log" && $file != "cgi-bin") {
            echo "<li><a href='$path/$file'>$file</a></li>";
            $i++;
        }
          }
          closedir($dh);
          ?>
        </ul>
  </div>

  </body>
</html>
